# Udemy Python Course #

Course material found here:

`https://www.udemy.com/course/complete-python-developer-zero-to-mastery/`

### Coding Tester

https://repl.it/

### Coding Standards

https://realpython.com/python-comments-guide/

### Python Cheat Sheet

https://github.com/aneagoie/ztm-python-cheat-sheet