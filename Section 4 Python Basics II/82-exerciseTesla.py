# 82-exerciseTesla.py
# Liam O'Brien 08/2020

"""
Checks the age of a driver to determine whether to start car
"""

def checkDriverAge(age=0):
    if int(age) < 18:
        print("Sorry, you are too young to drive this car. Powering off")
    elif int(age) > 18:
        print("Powering On. Enjoy the ride!");
    elif int(age) == 18:
        print("Congratulations on your first year of driving. Enjoy the ride!")

    return None


checkDriverAge(92)
checkDriverAge()