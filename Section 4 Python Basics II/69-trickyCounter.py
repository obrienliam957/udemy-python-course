# 69-trickyCounter.py
# Liam O'Brien 08/2020

"""
Exercise using looping to count items in a list
"""

def countSize (iterable):
    count = 0
    for value in iterable:
        count += 1
    return count


listToCount = [1,2,3,4,5,6,7,8,9,10]
setToCount = {1,2,3,4,5,6,7,8,9}
tupleToCount = (1,2,3,4,5,6,7,8,9,10,11)

print(f"There are {countSize(listToCount)} items in the list")
print(f"There are {countSize(setToCount)} items in the set")
print(f"There are {countSize(tupleToCount)} items in the tuple")