# 65-logicalOperators.py
# Liam O'Brien 08/2020

"""
Exercise to demonstrate logical operators by printing required text depending on conditions
"""

isMagician = bool(input("Are you a magician? (True/False) \t"))
isExpert = bool(input("Are you an expert? (True/False) \t"))

if isMagician and isExpert:
    print("You are a master magician")
elif isMagician and not isExpert:
    print("At least you're getting there")
elif not isMagician:
    print("You need maigical powers")
else:
    print("You've fallen through the gaps")