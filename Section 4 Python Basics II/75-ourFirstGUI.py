# 75-ourFirstGUI.py
# Liam O'Brien 08/2020

"""
Script to print out a matrix of 0,1 as space and * respectively
"""

picture = [
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 1, 1, 0, 0],
    [0, 1, 1, 1, 1, 1, 0],
    [1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0],
]

# Method 1
for row in picture:
    lineToRender = ""
    for pixel in row:
        if pixel:
            lineToRender += "*"
        else:
            lineToRender += " "
    print(lineToRender)

# Method 2
for row in picture:
    for pixel in row:
        if pixel:
            print("*", end="")
        else:
            print(" ", end="")
    print()

