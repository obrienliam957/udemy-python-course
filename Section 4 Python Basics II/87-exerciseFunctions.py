# 87-exerciseFunctions.py
# Liam O'Brien 08/2020


def highestEven(li):
    """
    Returns highest even number. Makes copy of list so no changes are made to original.
    """
    copyList = li.copy()
    copyList.sort()
    for number in reversed(copyList):
        if number % 2 == 0:
            return number


test=[8,10,1,2,3,4,5,8,11]
print(highestEven(test))
print(test)
