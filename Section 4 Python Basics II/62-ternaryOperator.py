# 62-ternaryOperator.py
# Liam O'Brien 08/2020

"""
Script showing use of terary operators
"""

isCondition = False

value = "true Condition" if isCondition else "false Condition"

print(value)