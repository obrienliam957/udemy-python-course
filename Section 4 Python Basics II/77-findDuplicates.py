# 77-findDuplicates.py
# Liam O'Brien 08/2020

"""
Find the duplicate entries of a list without creating sets
"""

someList = ['a', 'b', 'c', 'b', 'd', 'm', 'n', 'n']

# Method 1
uniqueList = []
duplicates1 = []

for entry in someList:
    if entry not in uniqueList:
        uniqueList.append(entry)
    elif entry not in duplicates1:
        duplicates1.append(entry)

print(f"The duplicate entries found from method 1 are: {duplicates1}")

# Method 2
duplicates2 = []

for entry in someList:
    if someList.count(entry) > 1:
        if entry not in duplicates2:
            duplicates2.append(entry)

print(f"The duplicate entries found from method 2 are: {duplicates2}")