# 37-typeConversion.py
# Liam O'Brien 08/2020

"""
Inputting birth year and responding with age
"""

from datetime import datetime

birthYearInput = input("What year were you born? \n")
birthYear = int(birthYearInput)
currentDate = datetime.now()

print(f"You are {currentDate.year - birthYear} years old.")