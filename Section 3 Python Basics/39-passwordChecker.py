# 39-passwordChecker.py
# Liam O'Brien 08/2020

"""
Python script to print out length of password
"""

username = input("Enter your username: ")
password = input("Enter your password: ")

passwordLength = len(password)
hiddenPassword = '*' * passwordLength

print(f"{username}, your password {hiddenPassword} is {passwordLength} letters long.")
