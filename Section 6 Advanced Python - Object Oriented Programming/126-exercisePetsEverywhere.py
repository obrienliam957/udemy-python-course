# 126-exercisePetsEverywhere.py
# Liam O'Brien 08/2020

"""
Exercise demonstrating inheritance.
"""

class Pets():
    animals = []
    def __init__(self, animals):
        self.animals = animals

    def walk(self):
        for animal in self.animals:
            print(animal.walk())

class Cat():
    is_lazy = True

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def walk(self):
        return f'{self.name} is just walking around'

# Showing how to call from parent components
class Simon(Cat):
    def __init__(self, name, age):
        super().__init__(name, age)

    def sing(self, sounds):
        return f'{sounds}'

class Sally(Cat):
    def sing(self, sounds):
        return f'{sounds}'

class Sickle(Cat):
    def sing(self, sounds):
        return f'{sounds}'

myCats = [Simon('Simon', 19), Sally('Sally', 3), Sickle('Sickle', 12)]

myPets = Pets(myCats)
myPets.walk()