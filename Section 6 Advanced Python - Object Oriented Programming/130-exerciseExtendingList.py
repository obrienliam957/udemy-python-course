# 130-exerciseExtendingList.py
# Liam O'Brien 08/2020

"""
Exercise to inherit base properties of list object and return constant value of 1000
for len(newObject)
"""

class SuperList(list):

    def __len__(self):
        return 1000

super_list = SuperList()
super_list.append(5)
print(super_list)
print(len(super_list))
