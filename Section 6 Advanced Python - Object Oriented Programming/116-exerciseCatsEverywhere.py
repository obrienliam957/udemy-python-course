# 116-exerciseCatsEverywhere.py
# Liam O'Brien 08/2020

"""
Using classes to create a cat object and find the oldest cat.
"""


class Cat:
    species = 'mammal'

    def __init__(self, name, age):
        self.name = name
        self.age = age

    @classmethod
    def example(cls, num1, num2):
        return cls('Ted', num1 + num2)

    @staticmethod
    def example2(num1, num2):
        return num1 + num2


def findOldestCat(*args):
    oldestCat = args[0]
    for cat in args:
        if cat.age > oldestCat.age:
            oldestCat = cat
    print(f"The oldest cat {oldestCat.name} is {oldestCat.age} years old")

cat1 = Cat('Cindy', 10)
cat2 = Cat('Fred', 9)
cat3 = Cat('Paula', 13)

findOldestCat(cat1, cat2, cat3)
